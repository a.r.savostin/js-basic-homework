// ## Задание

// Реализовать функцию подсветки нажимаемых клавиш. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - В файле `index.html` лежит разметка для кнопок.
// - Каждая кнопка содержит в себе название клавиши на клавиатуре
// - По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию `Enter` первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает `S`, и кнопка `S` окрашивается в синий цвет, а кнопка `Enter` опять становится черной.

// #### Литература:
// -  [Клавиатура: keyup, keydown, keypress](https://learn.javascript.ru/keyboard-events )

// ========================================================================

const btns = document.querySelector('.btn-wrapper').children;

window.addEventListener('keydown', (e) => {
    Array.from(btns).forEach(element => element.classList.remove('btn_blue'));

    // for (let btn of btns) {
    //     if (e.code === `Key${btn.textContent}` || e.code === btn.textContent) {
    //         btn.classList.add('btn_blue')
    //     }
    // }

    for (let btn of btns) {
        if (e.code === `Key${btn.getAttribute('data-key')}` || e.code === btn.getAttribute('data-key')) {
            btn.classList.add('btn_blue')
        }
    }
})