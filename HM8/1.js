// ## Задания
// --------------------------------------------------------------------------------
// ***********************************[ 1 ]****************************************
// --------------------------------------------------------------------------------

// Найти все параграфы на странице и установить цвет фона #ff0000

const allParagraphs = document.getElementsByTagName('p');
for (let elem of allParagraphs) {
    elem.style.backgroundColor = "#ff0000";
}


// --------------------------------------------------------------------------------
// ***********************************[ 2 ]****************************************
// --------------------------------------------------------------------------------

// Найти элемент с id="optionsList". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.

const idElem = document.getElementById('optionsList');
console.log(idElem);

const perentElement = idElem.parentElement;
console.log(perentElement);

console.log('======================================');

function elemChildNodes(element) {
    if (element.hasChildNodes()) {
        const childlNod = element.childNodes;

        for (let noda of childlNod) {
            console.log('Element: [' + noda.nodeName + '] nodeType: ' + noda.nodeType);
        }
    } else {
        console.log('This element has not childNodes');
    }
}
elemChildNodes(perentElement);


// --------------------------------------------------------------------------------
// ***********************************[ 3 ]****************************************
// --------------------------------------------------------------------------------

// Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = '<p>This is a paragraph</p>';


// --------------------------------------------------------------------------------
// ***********************************[ 4 ]****************************************
// --------------------------------------------------------------------------------

// Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль.
// Каждому из элементов присвоить новый класс nav-item.


console.log('======================================');

const mainHeader = document.querySelector('.main-header');
const allLiItems = mainHeader.querySelectorAll('li');
console.log(allLiItems);

allLiItems.forEach(elem => elem.classList.add('nav-item'))


// --------------------------------------------------------------------------------
// ***********************************[ 5 ]****************************************
// --------------------------------------------------------------------------------

// Найти все элементы с классом section-title. Удалить этот класс у элементов.

const allSectionTitle = document.querySelectorAll('.section-title');
allSectionTitle.forEach(elem => elem.classList.remove('section-title'))