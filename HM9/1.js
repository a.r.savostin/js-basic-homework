// ## Теоретический вопрос

// 1. Опишите каким образом можно создать новый HTML на странице.
// 2. Опишите что означает первый параметр функции insertAdjacentHTML и опишите возможные варианты этого параметра.
// 3. Как можно удалить элемент со страницы?

// ## Задание

// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

// #### Технические требования:

// - Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// - Каждый из элементов массива вывести на страницу в виде пункта списка;

// Примеры массивов, которые можно выводить на экран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можно взять любой другой массив.

// #### Необязательные задания продвинутой сложности:

// 1. Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив, выводить его как вложенный список.
//    Пример такого массива:

//    ```javascript
//    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//    ```

//    > Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.

// 2. Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.


// ************************************************************


const parentDiv = document.querySelector('.header_section');

const mas2 = ["1", "2", "3", "sea", "user", 23];
const mas3 = ["Arakus", "Kharkiv", "Kiev", ["Borispol", "Irpin"],
    "Odessa", "Lviv", "Dnieper", ['Krivbas', 'Zapor', 'Donetsk'], 'Mariupol'];

function showAray(arr, parent = document.body) {
    const newOlWrap = document.createElement('ol')

    arr.map(item => {
        const newListItem = document.createElement('li');

        if (Array.isArray(item)) {
            showAray(item, newListItem);
        } else {
            newListItem.textContent = `[ ${item} ]`;
        }
        newOlWrap.append(newListItem);
    });
    parent.append(newOlWrap);
}

showAray(mas3, parentDiv);

document.querySelector('.modal').classList.add('active');

let seconds = 3;
const count = document.getElementById('count_timer');
count.innerText = seconds;

let setInetrval = setInterval(() => {
    seconds--;
    count.innerText = seconds;

    if (seconds <= 0) {
        clearInterval(setInetrval);
        parentDiv.innerHTML = '';

        setTimeout(() => {
            document.querySelector('.modal').classList.remove('active');
        }, 700);
    }

}, 1000);
