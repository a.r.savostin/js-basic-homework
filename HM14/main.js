const mainImg = document.querySelector('.header_main_content_img');
const headerWraper = document.querySelector('.header_wraper');
const turnBtn = document.getElementById('turnBtn');
const toggleHend = document.querySelector('#toggleHend');
let hendAnimInterval;
const toShowImg = document.querySelector('.to-show');
const body = document.querySelector('body');
const blow = document.querySelector('.colorBlow')
const turnBtnSlog = document.querySelector('.turnBtn_slog')


if (localStorage.getItem('theme') === 'dark') {
    hendAnimInterval = setInterval(animHend, 600)
    lightTeme();
} else if (localStorage.getItem('theme') === 'light') {
    darkTheme();
}


turnBtn.addEventListener('click', () => {
    if (turnBtn.hasAttribute('data-vkl')) {
        lightTeme()
    } else {
        darkTheme()
    }
})

function lightTeme() {
    headerWraper.style.backgroundColor = '#000';
    turnBtn.src = './img/vkl1.svg';
    turnBtn.removeAttribute('data-vkl');
    turnBtn.setAttribute('data-vykl', '')
    mainImg.src = './img/col2.svg'
    clearInterval(hendAnimInterval);
    toggleHend.style.display = 'none';
    blow.classList.add('hidden');
    turnBtnSlog.classList.add('hidden')
    body.style.color = 'rgb(133, 133, 133)';
    localStorage.setItem('theme', 'dark')
}

function darkTheme() {
    turnBtn.src = './img/vkl2.svg';
    turnBtn.removeAttribute('data-vykl');
    turnBtn.setAttribute('data-vkl', '')
    mainImg.src = './img/col1.svg'
    headerWraper.style.backgroundColor = '#332848';
    toggleHend.style.display = 'block';
    body.style.color = '#fff';
    blow.classList.remove('hidden');
    turnBtnSlog.classList.remove('hidden')
    hendAnimInterval = setInterval(animHend, 600)
    localStorage.setItem('theme', 'light')
}

function animHend() {
    toggleHend.classList.add('animate')

    setTimeout(() => {
        toggleHend.classList.remove('animate')
    }, 400);
}

