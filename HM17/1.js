// ## Задание

// Создать объект "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - Создать пустой объект `student`, с полями `name` и `lastName`.
// - Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
// - В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента `tabel`.
// - Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение `Студент переведен на следующий курс`.
// - Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение `Студенту назначена стипендия`.

// #### Литература:
// - [Объекты как ассоциативные массивы](https://learn.javascript.ru/object)
// - [Перебор свойств объектов](https://learn.javascript.ru/object-for-in)

// ==================================================================================

const student = {
    name: '',
    lastName: '',
}

student.name = prompt('Enter your name');
student.lastName = prompt('Enter your last name');

let subj = prompt('Введіть назву предмету та оцінку від 1 до 12 (через тире  [ - ])');

let subjects = {};

while (subj !== null) {

    let subjSplit = subj.split('-');
    let newSubj = subjSplit[0].trim().toLowerCase();
    newSubj[0].toUpperCase();
    let subjValue = Number(subjSplit[1].trim());

    subjects[newSubj] = subjValue;

    subj = prompt('Введіть назву предмету та оцінку від 1 до 12 (через тире  [ - ])');
}

student.subjects = subjects;
console.log(student);


let lowerVal = []

for (let values of Object.values(student.subjects)) {

    if (values <= 4) {
        lowerVal.push(values);
    }
}

if (lowerVal.length < 4) {
    alert('Студент переведен на следующий курс');
}

let sumVal = Object.values(student.subjects).reduce((a, b) => a += b)

if ((sumVal / Object.values(student.subjects).length) > 7) {
    alert('Студенту назначена стипендия, так как средний бал больше 7');
}




