// ## Завдання

// Реалізувати функцію фільтру масиву за вказаним типом даних.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Написати функцію`filterBy()`, яка прийматиме 2 аргументи.Перший аргумент - масив, який міститиме будь - які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.Тобто якщо передати масив['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив[23, null].


// #### Література:

// -[Масиви з числовими індексами](http://learn.javascript.ru/array)
//     -[Масив: перебираючі методи](http://learn.javascript.ru/array-iteration)
//         -[Шість типів даних, typeof](https://learn.javascript.ru/types-intro)


// ****************************************

const arg1 = [
    'hello',
    'world',
    23,
    '23',
    null,
    company = {
        a: 'google',
        b: 'apple',
        c: 'intel',
    },
    user = {
        name: "Alex",
        lastName: "Savostin",
        age: 33,
    },
    "const",
    "Super puper",
    14,
    22,
];
const arg2 = 'number';

function filterBy(array, typeOfelem) {
    if (Array.isArray(array)) {
        return array.filter(element => typeof (element) === typeOfelem);
    } else {
        console.log("Sorry, is not Array");
    }
}

const newArr = filterBy(arg1, arg2);
console.log(newArr);

// console.log(filterBy(arg1, arg2));

