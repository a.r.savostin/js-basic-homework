const ourServicesTabList = document.querySelector('.our_services_tab_list');
const ourServicesTabText = document.querySelector('.our_services_tab_text');
const ourServicesTabContentWrap = document.querySelector('.our_services_tab_content_wrap');

const ourServicesTabImg = document.querySelector('.our_services_tab_content_img');
let tabContentImg = document.querySelector('#tabContentImg');
let tabContentText = document.querySelector('#tabContentText');
let tabsContent

// ==== tab contetn OBJ ======
tabsContent = [
    tab1 = {
        title: 'Web Design',
        img: {
            url: './imgs/Layer 9 1.png',
            alt: 'img Web Design',
        },
        decription: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis Lorem ipsum dolor sit amet consectetur adipisicing elit",
    },
    tab2 = {
        title: 'Graphic Design',
        img: {
            url: './imgs/Layer 1 5.png',
            alt: 'img Graphic Design',
        },
        decription: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit ",
    },
    tab3 = {
        title: 'Online Support',
        img: {
            url: './imgs/Layer 31.png',
            alt: 'img Online Support',
        },
        decription: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
    },
    tab4 = {
        title: 'App Design',
        img: {
            url: './imgs/Layer 32.png',
            alt: 'img App Design',
        },
        decription: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis. ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet",
    },
    tab5 = {
        title: 'Web Design',
        img: {
            url: './imgs/Layer 34.png',
            alt: 'img Web Design',
        },
        decription: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis. illo assumenda amet",
    },
    tab6 = {
        title: 'Seo Service',
        img: {
            url: './imgs/Layer 9 1.png',
            alt: 'img Seo Service',
        },
        decription: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet, sit vitae libero reprehenderit tempora ipsum aperiam facilis. ipsum dolor sit amet consectetur adipisicing elit. Consequuntur modi recusandae cupiditate consequatur suscipit  quo laborum corrupti iure molestiae illo assumenda amet",
    },
]
// ===========================

function getTabsBlock() {
    const checkedTab = 0;
    const getServicesTabs = document.querySelector('#getServicesTabs');

    if (tabsContent === undefined || tabsContent.length <= 0) {
        getServicesTabs.style.display = 'none';
        return
    } else {

        for (let elem = 0; elem < tabsContent.length; elem++) {
            const li = document.createElement('li');
            li.classList.add('services_tab_item');
            li.textContent = tabsContent[elem].title;
            ourServicesTabList.append(li);
        }
        if (tabsContent.length > 0) {
            ourServicesTabList.children[checkedTab].classList.add('active');
            tabContentImg.src = tabsContent[checkedTab].img.url;
            tabContentImg.setAttribute('alt', tabsContent[checkedTab].img.alt)
            tabContentText.textContent = tabsContent[checkedTab].decription;
        }

        clickTab();
    }
}

function getActiveTabContent() {
    Array.from(ourServicesTabList.children).forEach((el, index) => {
        if (el.classList.contains('active')) {
            tabContentImg.src = tabsContent[index].img.url;
            tabContentText.textContent = tabsContent[index].decription;
            tabContentImg.setAttribute('alt', tabsContent[index].img.alt)
        }
        return
    });
}

function clickTab() {
    ourServicesTabList.addEventListener('click', (e) => {
        for (let item of ourServicesTabList.children) {
            item.classList.remove('active')
        }
        e.target.classList.add('active')

        getActiveTabContent()

    })
}

getTabsBlock();


// ====== WORKS IMG DATA ========
let ourAmazingWork = [
    { src: './imgs/our works/L24.png', title: 'A4 paper', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l25.png', title: 'Red tablets', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l27.png', title: 'some gollage', category: 'Web Design', link: '#', },
    { src: './imgs/our works/l28.png', title: 'Boxes are white', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l29.png', title: 'Jornal', category: 'Web Design', link: '#', },
    { src: './imgs/our works/l30.png', title: 'Boxes are black', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/l31.png', title: 'some gollage', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/l32.png', title: 'some gollage', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l33.png', title: 'CD designe', category: 'Wordpress', link: '#', },
    { src: './imgs/our works/l34.png', title: 'Jornal', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l26.png', title: 'Cup designe', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/l36.png', title: 'Boxes are sepia', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/L24.png', title: 'A4 paper', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l25.png', title: 'Red tablets', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l27.png', title: 'some gollage', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l28.png', title: 'Boxes are white', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l29.png', title: 'Jornal', category: 'Wordpress', link: '#', },
    { src: './imgs/our works/l30.png', title: 'Boxes are black', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/l31.png', title: 'some gollage', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/l32.png', title: 'some gollage', category: 'Graphic Design', link: '#', },
    { src: './imgs/our works/l33.png', title: 'CD designe', category: 'Wordpress', link: '#', },
    { src: './imgs/our works/l34.png', title: 'Jornal', category: 'Wordpress', link: '#', },
    { src: './imgs/our works/l26.png', title: 'Cup designe', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/l36.png', title: 'Boxes are sepia', category: 'Landing Pages', link: '#', },
    { src: './imgs/our works/webd1.jpg', title: 'webd1', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd2.png', title: 'webd2', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd3.jpg', title: 'webd3', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd4.jpg', title: 'webd4', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd5.jpg', title: 'webd5', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd6.jpg', title: 'webd6', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd7.jpg', title: 'webd7', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd8.png', title: 'webd8', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd9.png', title: 'webd9', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd10.gif', title: 'webd10', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd11.gif', title: 'webd11', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd12.jpeg', title: 'webd12', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd13.png', title: 'webd13', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd14.jpg', title: 'webd14', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd15.png', title: 'webd15', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd16.jpeg', title: 'webd16', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd17.jpeg', title: 'webd17', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd18.jpg', title: 'webd18', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd19.jpg', title: 'webd19', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd20.png', title: 'webd20', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd21.jpg', title: 'webd21', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd22.png', title: 'webd22', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd23.png', title: 'webd23', category: 'Web Design', link: '#', },
    { src: './imgs/our works/webd24.jpg', title: 'webd24', category: 'Web Design', link: '#', },
]
// ==============================

const amazingWorkImgConteiner = document.getElementById('amazingWorkImgConteiner');
const btnLoadMoreWorksImg = document.getElementById('loadMoreWorksImg');
const amazingWorkTabsList = document.getElementById('amazingWorkTabsList');
let activeTabCategory = 'All';
let lastCount = 0;
const howManyImgsShow = 12;

crateAmazingWorkTabsList();
ShowImgs(howManyImgsShow, ourAmazingWork);


// create tabs list from data
function crateAmazingWorkTabsList() {

    const categorys = [...new Set(ourAmazingWork.map(elem => elem.category.toLowerCase()))];

    const newLi = document.createElement('li');
    newLi.classList.add('amazing_work_tab_item');
    newLi.classList.add('active')
    newLi.textContent = 'All';
    amazingWorkTabsList.append(newLi)

    for (let i = 0; i < categorys.length; i++) {
        const newLi = document.createElement('li');
        newLi.classList.add('amazing_work_tab_item');

        newLi.textContent = categorys[i];
        amazingWorkTabsList.append(newLi)
    }

    clickAmazingWorkTabs()
}

function ShowImgs(count, array) {

    if (count > array.length - lastCount) {
        count = array.length - lastCount;
    }

    for (i = 0; i < count; i++) {
        const newDiv = document.createElement('div');
        const newImg = document.createElement('img');
        newImg.src = array[lastCount + i].src; // ****
        newDiv.classList.add('amazing_work_tab_img_wrap');
        newDiv.appendChild(newImg);

        const newDivInfoWrap = document.createElement('div');
        newDivInfoWrap.classList.add('amazing_work_tab_img_info_wrap')

        const newP_title = document.createElement('p');
        newP_title.classList.add('work_tab_img_litle');
        newP_title.textContent = array[lastCount + i].title; // *****

        const newP_category = document.createElement('p');
        newP_category.classList.add('work_tab_img_category');
        newP_category.textContent = array[lastCount + i].category.toLowerCase(); // *****

        const hrafDiv = document.createElement('div');
        hrafDiv.classList.add('href_div')
        const newLink = document.createElement('a');
        const newLinkStop = document.createElement('a');
        newLinkStop.setAttribute('data-stop', '')
        newLink.href = array[lastCount + i].link; // *****
        newLinkStop.href = '#';
        hrafDiv.appendChild(newLink);
        hrafDiv.appendChild(newLinkStop);
        newDivInfoWrap.appendChild(hrafDiv)
        newDivInfoWrap.appendChild(newP_title);
        newDivInfoWrap.appendChild(newP_category);

        newDiv.appendChild(newDivInfoWrap);

        amazingWorkImgConteiner.append(newDiv);

        if (array.length <= (lastCount + count)) {
            btnLoadMoreWorksImg.style.display = 'none';
        }
    }
    lastCount = lastCount + count;

    amazingWorkImgConteiner.addEventListener('click', e => {
        if (e.target.hasAttribute('data-stop')) {
            e.preventDefault();
            let div = e.target.parentElement.parentElement
            div.style.top = '100%';
            setTimeout(() => div.removeAttribute('style'), 2000);
        }
    })

}


// add click actoin on tabs
function clickAmazingWorkTabs() {
    amazingWorkTabsList.addEventListener('click', e => {
        Array.from(amazingWorkTabsList.children).forEach(el => el.classList.remove('active'));
        e.target.classList.add('active')
        activeTabCategory = e.target.textContent
        filtredCategorys = ourAmazingWork.filter(el => el.category.toLowerCase() === activeTabCategory)

        if (activeTabCategory !== e.target) {
            btnLoadMoreWorksImg.style.display = 'block';
        }

        if (activeTabCategory === 'All') {
            amazingWorkImgConteiner.innerHTML = ''
            lastCount = 0;
            ShowImgs(howManyImgsShow, ourAmazingWork)
        } else {
            amazingWorkImgConteiner.innerHTML = ''
            lastCount = 0;
            ShowImgs(howManyImgsShow, filtredCategorys)
        }

    })
}


// add click on add more img
btnLoadMoreWorksImg.addEventListener('click', (e) => {
    const loudAnimElement = document.querySelector('.loaded');
    const lodingDataWrap = document.querySelector('.loding_data_wrap')
    loudAnimElement.classList.add('active')
    lodingDataWrap.classList.remove('hidden')

    function addImgs() {
        if (activeTabCategory !== 'All') {
            ShowImgs(howManyImgsShow, filtredCategorys)
        } else {
            ShowImgs(howManyImgsShow, ourAmazingWork)
        }
        lodingDataWrap.classList.add('hidden')
        loudAnimElement.classList.remove('active')
    }
    setTimeout(addImgs, 3000);

})


// ===== PEOLES =====
const peoples = [
    {
        name: 'Iuda',
        workPositin: 'UX Designer',
        avatar: './imgs/Layer 6.png',
        comment: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer dignissim, augue tempus dictum odio nisi quis massa. Integer dignissim"
    },
    {
        name: 'Nadin',
        workPositin: 'Front-End Developer',
        avatar: './imgs/Layer 8.png',
        comment: "Aaugue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer dignissim, augue  ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer dignissim"
    },
    {
        name: 'Savostin Alex',
        workPositin: 'Front-End Developer',
        avatar: './imgs/alex.jpg',
        comment: "Wow! wow! wow! mother facker =)"
    },
    {
        name: 'Bro',
        workPositin: 'Back-End Developer',
        avatar: './imgs/L344.png',
        comment: "Integer , augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer , augue tempus  luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer dignissim"
    },
    {
        name: 'Not-Bro',
        workPositin: 'Not Developer',
        avatar: './imgs/Layer 7.png',
        comment: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Integer dignissim"
    },
]
// ==================

let zoomAvatar = document.querySelector('#zoomAvatar');
let activeComment = document.querySelector('#activeComment');
let activeName = document.querySelector('#activeName');
let ActivWorkPosition = document.querySelector('#ActivWorkPosition');
const peoplesAvatarsWrap = document.querySelector('.peoples_avatars_wrap');
const avatarSliderConteiner = document.querySelector('.people_avatar_img_slider_conteiner');
const pagLeftBtn = document.querySelector('#pagLeft');
const pagRightBtn = document.querySelector('#pagRight');
const animUnderRound = document.querySelector('.anim_under_round')
const slideConteiner = document.querySelector('.slider_conteiner')

createPeopleList()

function createPeopleList() {
    for (i = 0; i < peoples.length; i++) {
        const newDiv = document.createElement('div');
        const newAvatar = document.createElement('img');
        newDiv.classList.add('people_avatar_img_wrap');
        newAvatar.src = peoples[i].avatar;
        newDiv.appendChild(newAvatar);
        avatarSliderConteiner.appendChild(newDiv);
    }
    avatarSliderConteiner.children[2].classList.add('active')
}

const slideArray = Array.from(avatarSliderConteiner.children);
let ectElemetn = slideArray.findIndex(el => el.classList.contains('active'))


function chengeContent() {
    ectElemetn = slideArray.findIndex(el => el.classList.contains('active'))
    zoomAvatar.firstElementChild.src = peoples[ectElemetn].avatar;
    activeName.textContent = peoples[ectElemetn].name;
    activeComment.textContent = peoples[ectElemetn].comment;
    ActivWorkPosition.textContent = peoples[ectElemetn].workPositin;
}


function activateAvatarOnClick(e) {
    slideArray.forEach(el => el.classList.remove('active'));
    e.target.parentElement.classList.add('active');
    ectElemetn = slideArray.findIndex(el => el.classList.contains('active'))

    //!!!!!!!!!!!!!!!! <==
    Array.from(slideConteiner.children).forEach(el => el.classList.remove('active'));
    slideConteiner.children[ectElemetn].classList.add('active');
    getActiveSlidesDiv()
    mooveImgsLR();

}

function slideLeft() {
    const lastOfarr = avatarSliderConteiner.children.length - 1;
    ectElemetn = slideArray.findIndex(el => el.classList.contains('active'))

    if (ectElemetn > 0) {
        slideArray[ectElemetn].classList.remove('active');
        slideArray[ectElemetn - 1].classList.add('active');
    }

    if (ectElemetn === 0) {
        slideArray[ectElemetn].classList.remove('active');
        slideArray[lastOfarr].classList.add('active');
    }
    slideDivL()
}

function slideRight() {
    const lastOfarr = avatarSliderConteiner.children.length - 1;
    ectElemetn = slideArray.findIndex(el => el.classList.contains('active'))

    if (ectElemetn === lastOfarr) {
        slideArray[ectElemetn].classList.remove('active');
        slideArray[0].classList.add('active');
    }

    if (ectElemetn < lastOfarr) {
        slideArray[ectElemetn].classList.remove('active');
        slideArray[ectElemetn + 1].classList.add('active');
    }
    slideDivR()
}


pagLeftBtn.addEventListener('click', e => slideLeft())
pagRightBtn.addEventListener('click', e => slideRight())
avatarSliderConteiner.addEventListener('click', e => activateAvatarOnClick(e))


function createPeopleSeyContentSlider() {

    for (let i = 0; i < slideArray.length; i++) {

        const divSlide = document.createElement('div');
        divSlide.classList.add('slide_content');
        const seyPic = document.createElement('img');
        seyPic.src = './imgs/comment_icon.svg';
        seyPic.classList.add('comment_icon');
        const parapraphCommetn = document.createElement('p');
        parapraphCommetn.classList.add('people_comment_say');
        parapraphCommetn.textContent = peoples[i].comment // ****
        const parapraphName = document.createElement('p');
        parapraphName.classList.add('name_of_active_peaole');
        parapraphName.textContent = peoples[i].name // ***
        const parapraphProfeccion = document.createElement('p');
        parapraphProfeccion.textContent = peoples[i].workPositin// ***
        parapraphProfeccion.classList.add('porofetion_of_active_people');

        const divImgAvaterWrap = document.createElement('div');
        divImgAvaterWrap.classList.add('active_avatar');
        const avatarToSlide = document.createElement('img');
        avatarToSlide.src = peoples[i].avatar // ***

        divSlide.appendChild(seyPic);
        divSlide.appendChild(parapraphCommetn);
        divSlide.appendChild(parapraphName);
        divSlide.appendChild(parapraphProfeccion);
        divImgAvaterWrap.appendChild(avatarToSlide)
        divSlide.appendChild(divImgAvaterWrap);

        slideConteiner.appendChild(divSlide)
    }

    slideConteiner.children[ectElemetn].classList.add('active')
}

createPeopleSeyContentSlider();

let slideDivs = Array.from(slideConteiner.children)
let activeDiv = 0;

getActiveSlidesDiv()

function getActiveSlidesDiv() {
    slideDivs.forEach((el, index) => {
        if (el.classList.contains('active')) {
            activeDiv = index;
        }
    });
}


function slideDivR() {

    if (activeDiv === slideDivs.length - 1) {
        slideDivs[activeDiv].classList.remove('active');
        slideDivs[0].classList.add('active');
        getActiveSlidesDiv();
        mooveImgsLR();
    } else {
        slideDivs[activeDiv + 1].classList.add('active');
        slideDivs[activeDiv].classList.remove('active');

        getActiveSlidesDiv();
        mooveImgsLR();
    }

}

function slideDivL() {

    if (activeDiv === 0) {
        slideDivs[activeDiv].classList.remove('active');
        slideDivs[slideDivs.length - 1].classList.add('active');
        getActiveSlidesDiv();
        mooveImgsLR();
    } else {
        slideDivs[activeDiv - 1].classList.add('active');
        slideDivs[activeDiv].classList.remove('active');

        getActiveSlidesDiv();
        mooveImgsLR();
    }

}

function mooveImgsLR() {
    slideDivs.forEach((el, index, arr) => {

        if (index === activeDiv) {
            el.style.transform = 'translateX(0)'
        }
        if (index < activeDiv) {
            el.style.transform = 'translateX(-100%)'
        }
        if (index > activeDiv) {
            el.style.transform = 'translateX(+100%)'
        }
    })
}

mooveImgsLR();



// Masonry

let masonryElements = document.querySelectorAll('.mas_img_wrap');

masonryElements.forEach((elem, index) => {
    let img = elem.firstElementChild;
    startCount = 0
})

const countCollumns = 3
let nextChunk;
let arrayForStyleTop = []

function getHeightArrElem() {

    for (i = 0; i < countCollumns; i += countCollumns) {
        const chunk = Array.from(masonryElements).slice(i, i + countCollumns);
        let ArraysHeight = chunk.map(elem => elem.firstElementChild.offsetHeight);

        let maxHeight = Math.max(...ArraysHeight);

        // console.log(ArraysHeight);

        ArraysHeight.forEach((elem, index) => {
            let resultHeight = elem - maxHeight
            arrayForStyleTop.push(resultHeight)

            // console.log(resultHeight);
        })

        // console.log('-------[ first row ]----------');

    }

    for (i = countCollumns; i < masonryElements.length; i += countCollumns) {
        nextChunk = Array.from(masonryElements).slice(i, i + countCollumns);

        // nextChunk = 0 0 0 - first row
        // nextChunk =

        let ArraysHeight = nextChunk.map(elem => elem.firstElementChild.offsetHeight);

        // console.log(ArraysHeight);

        let maxHeight = Math.max(...ArraysHeight);
        // console.log(maxHeight);

        // console.log(arrayForStyleTop);

        ArraysHeight.forEach((elem, index) => {
            let resultHeight = elem - maxHeight
            arrayForStyleTop.push(resultHeight)

            // console.log(arrayForStyleTop);
        })

        // console.log('-----------------');
    }

}

getHeightArrElem();


function masonryTopElemetns() {

    for (i = 0; i < countCollumns; i++) {
        arrayForStyleTop.unshift(0);
    }

    for (i = 0; i < countCollumns * 2; i++) {
        masonryElements[i].classList.add(`el${i}`)

        if (masonryElements[i].classList.contains(`el${i}`)) {
            masonryElements[i].style.top = arrayForStyleTop[i] + 'px';
        }
    }

    for (i = countCollumns * 2; i < masonryElements.length; i++) {
        masonryElements[i].classList.add(`el${i}`)

        if (masonryElements[i].classList.contains(`el${i}`)) {
            arrayForStyleTop[i] += arrayForStyleTop[i - countCollumns]

            masonryElements[i].style.top = arrayForStyleTop[i] + 'px'
        }
    }
}

masonryTopElemetns();