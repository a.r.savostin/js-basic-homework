// ## Задание

// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:

// - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.

// #### Литература:

// - [Объекты как ассоциативные массивы](https://learn.javascript.ru/object)
// - [Object.defineProperty()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)


// ****** Основне завдання ******

function createNewUser() {

    let firstName = prompt("Enter your first name").trim();
    let lastName = prompt("Enter your last name").trim();

    return {
        firstName,
        lastName,
        setnewFirstName: function (newvalue) {
            Object.defineProperty(this, 'firstName', {
                value: newvalue
            })
        },
        setnewLasttName: function (newvalue) {
            Object.defineProperty(this, 'lastName', {
                value: newvalue
            })
        },
    }

};

const newUser = createNewUser();

Object.defineProperties(newUser, {
    'firstName': {
        writable: false,
        enumerable: true,
        configurable: true,
    },
    'lastName': {
        writable: false,
        enumerable: true,
        configurable: true,
    }
});

newUser.getLogin = function () {
    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
}

console.log(newUser);
console.log(newUser.getLogin());

newUser.setnewFirstName("Roma");
console.log(newUser);


