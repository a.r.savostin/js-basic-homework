// ### Дане завдання не обов'язкове для виконання

// ## Завдання

// Реалізувати універсальний фільтр масиву об'єктів. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Написати функцію `filterCollection()`, яка дозволить відфільтрувати будь-який масив за заданими ключовими словами.
// - Функція має приймати на вхід три основні аргументи:
//   - масив, який треба відфільтрувати
//   - Рядок з ключовими словами, які треба знайти всередині масиву (одне слово або кілька слів, розділених пробілом)
//   - boolean прапор, який буде говорити, чи треба знайти всі ключові слова (`true`), або достатньо збігу одного з них (`false`)
//   - четвертий і наступні аргументи будуть іменами полів, у яких треба шукати збіг. Якщо поле не на першому рівні об'єкта, до нього треба вказати повний шлях через `.`. Рівень вкладеності полів може бути будь-яким.
// - Приклад виклику функції:

// ```javascript
// filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description')
// ```
//    В даному прикладі буде відфільтрований масив `vehicles`, за допомогою ключових слів `en_US` та `Toyota`. `true` у третьому параметрі означає, що для успішного включення запису у фінальний результат має бути збіг за обома ключовими словами. Останні кілька параметрів містять імена полів, у яких треба шукати ключові слова. Наприклад `contentType.name` означає, що всередині кожного об'єкта `vehicle` може бути поле `contentType`, яке є об'єктом або масивом об'єктів, усередині яких може бути поле `name`.Именно в этом поле (а также в других указанных) необходимо искать сопадения с ключевыми словами.
// - У прикладі вище - запис `locales.name` означає, що поле `locales` всередині об'єкта `vehicle` може бути як об'єктом, так і масивом. Якщо воно є масивом, це означає, що всередині масиву знаходяться об'єкти, у кожного з яких може бути властивість `name`. Для успішної фільтрації достатньо знаходження ключового слова хоча б у одному з елементів масиву.
// - Різні ключові слова можуть бути в різних властивостях об'єкта.Наприклад, у прикладі вище, ключове слово `en_US` може бути знайдено в полі `locales.name`, тоді як ключове слово `Toyota` може, наприклад, знаходитися всередині властивості `description`. При цьому такий об'єкт має бути знайдено.
// - Пошук має бути нечутливим до регістру.

// ##### Примітка:
// Реалізацію цього завдання можна використовувати у реальному житті. Наприклад, якщо на сторінці є таблиця з даними, а вгорі є рядок пошуку, цю функцію можна використовувати для фільтру значень у таблиці при введенні ключових слів у рядок пошуку

// #### Література:
// - [Масиви з числовими індексами](http://learn.javascript.ru/array)
// - [Масив: перебираючі методи](http://learn.javascript.ru/array-iteration)
// - [Деструктуризація](http://learn.javascript.ru/destructuring)
// - [Array.isArray()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray)


// ***************

let cars = [
    {
        brand: 'audi',		//фирма производитель
        model: 'a4',		//модель
        volume_engine: '1.8',	//объём двигателя
        hp: '120',		//кол-во лошадиных сил
        awd: 'нет',   		//полный привод [да/нет]
        automat: 'да',		//автомат [да/нет]
    },
    {
        brand: 'audi',		//фирма производитель
        model: 'a4 allroad',	//модель
        volume_engine: '2',	//объём двигателя
        hp: '211',		//кол-во лошадиных сил
        awd: 'да',   		//полный привод [да/нет]
        automat: 'да',		//автомат [да/нет]
    },
    {
        brand: 'audi',		//фирма производитель
        model: 'a6',		//модель
        volume_engine: '2',	//объём двигателя
        hp: '180',		//кол-во лошадиных сил
        awd: 'нет',   		//полный привод [да/нет]
        automat: 'да',		//автомат [да/нет]
    },
    {
        brand: 'bmw',		//фирма производитель
        model: '3 Series',	//модель
        volume_engine: '1.6',	//объём двигателя
        hp: '135',		//кол-во лошадиных сил
        awd: 'нет',   		//полный привод [да/нет]
        automat: 'нет',		//автомат [да/нет]
    },
    {
        brand: 'bmw',		//фирма производитель
        model: '5 Series',	//модель
        volume_engine: '3',	//объём двигателя
        hp: '258',		//кол-во лошадиных сил
        awd: 'нет',   		//полный привод [да/нет]
        automat: 'да',		//автомат [да/нет]
    },
    {
        brand: 'volkswagen',	//фирма производитель
        model: 'passat',	//модель
        volume_engine: '1.8',	//объём двигателя
        hp: '152',		//кол-во лошадиных сил
        awd: 'нет',   		//полный привод [да/нет]
        automat: 'да',		//автомат [да/нет]
    },
]

// function filterCollection(array, key, bool) {
//     const resultFilterColection = [];
//     const filterKeys = key.trim().split(' ');
//     console.log(filterKeys);

//     for (let elem of array) {
//         // console.log(Object.keys(elem));
//         if (Object.keys(elem).includes(filterKeys)) {

//         } else { false }
//     }

//     return
// }

// filterCollection(cars, 'brand model', true)

function filterCollection(collection, keywords, matchAll, ...fields) {
    const keywordsArray = keywords.split(' ');
    const filteredCollection = collection.filter(item => {
        const matches = fields.map(field => {
            const fieldValue = field.split('.').reduce((obj, key) => obj?.[key], item);
            if (fieldValue) {
                const stringifiedValue = JSON.stringify(fieldValue);
                return keywordsArray.every(keyword => stringifiedValue.includes(keyword));
            }
            return false;
        });
        return matchAll ? matches.every(Boolean) : matches.some(Boolean);
    });
    return filteredCollection;
}

console.log(filterCollection(cars, 'brand model', true, 'automat'))