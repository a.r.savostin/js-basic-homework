// ## Задание

// Реализовать переключение вкладок (табы) на чистом Javascript.

// #### Технические требования:
// - В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// - Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

// #### Литература:
// - [Использование data-* атрибутов](https://developer.mozilla.org/ru/docs/Learn/HTML/Howto/Use_data_attributes)


// **********************************

const tabs = document.querySelector('.tabs');
const tabsContetnt = document.querySelector('.tabs-content').children;

// зробив так щоб була актична перша табка (не пропісуючи в html)
tabs.children[0].classList.add('active')

// шукаємо відповідній текст до активної табки і робимо текст видимим
for (item of tabs.children) {
    if (item.classList.contains('active')) {
        for (itemContent of tabsContetnt) {
            if (itemContent.getAttribute('data-parent') === item.textContent) {
                itemContent.classList.add('active')
            }
        }
    }
}

// по кліку перемикаємо табки та контент
tabs.addEventListener('click', function (elem) {
    Array.from(tabs.children).forEach(elem => elem.classList.remove('active'));
    elem.target.classList.add('active');

    for (let item of tabsContetnt) {
        if (elem.target.textContent == item.getAttribute('data-parent')) {
            Array.from(tabsContetnt).forEach(element => element.classList.remove('active'));
            item.classList.add('active')
        };
    }
})
