// ## Задание

// Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - В папке `banners` лежит HTML код и папка с картинками.
// - При запуске программы на экране должна отображаться первая картинка.
// - Через 3 секунды вместо нее должна быть показана вторая картинка.
// - Еще через 3 секунды - третья.
// - Еще через 3 секунды - четвертая.
// - После того, как покажутся все картинки - этот цикл должен начаться заново.
// - При запуске программы где-то на экране должна появиться кнопка с надписью `Прекратить`.
// - По нажатию на кнопку `Прекратить` цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// - Рядом с кнопкой `Прекратить` должна быть кнопка `Возобновить показ`, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

// #### Необязательное задание продвинутой сложности:
// - При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
// - Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.

// ====================================================================

const imgWraper = document.querySelector('.images-wrapper');
const imgs = imgWraper.children

const btnsWrap = document.querySelector('.btns_wrap')
const imgPath = Array.from(imgs).map(e => e.getAttribute('src'))
const showSlideImg = document.createElement('img');
let secInterval = 3;
let index = 0
let time = secInterval - 1
let milisec = 10;
let playSlideImgs
let startTimer
const playBtn = document.querySelector('#play')


function slideInterval() {
    index++
    if (index >= imgPath.length) {
        index = 0;
    }
    showSlideImg.src = imgPath[index]
}

function onClickStart() {
    imgWraper.style.justifyContent = 'center'

    Array.from(imgs).forEach(e => {
        e.classList.add('hidden')
    });

    btnsWrap.querySelector('#stop').classList.remove('hidden');
    btnsWrap.querySelector('#play').classList.remove('hidden');
    btnsWrap.querySelector('#start').classList.add('hidden');


    if (!imgWraper.querySelector('.show_img_slider')) {
        showSlideImg.classList.add('show_img_slider');
        showSlideImg.src = imgPath[index];
        imgWraper.append(showSlideImg);
        timerWrap.style.display = 'flex';
    }
    playBtn.setAttribute('disabled', '');
    playSlideImgs = setInterval(slideInterval, secInterval * 1000);
    startTimer = setInterval(Timer, 100)
}


function onClickStop() {
    clearInterval(playSlideImgs)
    if (imgWraper.querySelector('.show_img_slider')) {
        imgWraper.querySelector('.show_img_slider').remove()
    }
    Array.from(imgs).forEach(el => {
        el.classList.remove('hidden')
    });
    imgWraper.style.justifyContent = 'space-between';
    timerWrap.style.display = 'none';
    playBtn.removeAttribute('disabled', '');
    clearInterval(playSlideImgs);
    clearInterval(startTimer);
    time = secInterval - 1
    milisec = 10;
}


btnsWrap.addEventListener('click', function (e) {
    if (e.target.id === 'start') {
        onClickStart();
    }
    if (e.target.id === 'stop') {
        onClickStop();
    }
    if (e.target.id === 'play') {
        onClickStart();
    }
})

const timerWrap = document.querySelector('.temer_wrap');
let secHtml = document.querySelector('#sec');
let milSecHtml = document.querySelector('#mil_sec');
let milSecInterval;



function Timer() {
    if (time === 0 && milisec === 0) {
        time = secInterval;
    }
    if (milisec <= 0) {
        time--
        milisec = 10;
    }
    milisec--

    if (time < 9) {
        secHtml.textContent = '0' + time;
    } else {
        secHtml.textContent = time;
    }
    if (milisec < 10) {
        milSecHtml.textContent = '0' + milisec;
    } else {
        milSecHtml.textContent = milisec
    }
}