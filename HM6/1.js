// ## Задание

// Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
//   1.  При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
//   2.  Создать метод `getAge()` который будет возвращать сколько пользователю лет.
//   3.  Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
// - Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.

// **********************************

function createNewUser() {

    let firstName = prompt("Enter your first name").trim();
    let lastName = prompt("Enter your last name").trim();
    let birthday = prompt("Enter your birthday like thet: dd.mm.yyyy").trim().split('.');
    let dayNow = new Date();

    return {
        firstName,
        lastName,
        birthday: {
            dd: Number(birthday[0]),
            mm: Number(birthday[1]),
            yy: Number(birthday[2]),
        },
        dateNowObj: {
            dd: dayNow.getDate(),
            mm: dayNow.getMonth() + 1,
            yy: dayNow.getFullYear(),
        },

        getAge: function () {
            if (this.birthday.mm > this.dateNowObj.mm) {
                return this.dateNowObj.yy - this.birthday.yy - 1
            }
            else if (this.birthday.mm === this.dateNowObj.mm) {
                if (this.birthday.dd > this.dateNowObj.dd) {
                    return this.dateNowObj.yy - this.birthday.yy - 1
                } return this.dateNowObj.yy - this.birthday.yy
            }
            else {
                return this.dateNowObj.yy - this.birthday.yy
            }
        },

        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getPassword: function () {
            return this.firstName[0].toUpperCase() +
                this.lastName.toLowerCase() +
                this.birthday.yy
        },

        setnewFirstName: function (newvalue) {
            Object.defineProperty(this, 'firstName', {
                value: newvalue
            })
        },

        setnewLasttName: function (newvalue) {
            Object.defineProperty(this, 'lastName', {
                value: newvalue
            })
        },
    }

};

const newUser = createNewUser();

Object.defineProperties(newUser, {
    'firstName': {
        writable: false,
        enumerable: true,
        configurable: true,
    },
    'lastName': {
        writable: false,
        enumerable: true,
        configurable: true,
    }
});


console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());