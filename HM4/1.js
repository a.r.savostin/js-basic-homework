// ## Задание

// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Считать с помощью модального окна браузера два числа.
// - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.
// - Создать функцию, в которую передать два значения и операцию.
// - Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:

// - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


// ****** { answer } *******

const firstNumberMessege = "Enter first number";
const secondNumberMessege = "Enter second number";
let num1;
let num2;

function checkNumbers() {
    num1 = prompt(firstNumberMessege);
    num2 = prompt(secondNumberMessege);

    while (isNaN(num1) || isNaN(num2) || num1, num2 == "" || num1, num2 === null) {
        alert("Please enter a number");
        num1 = prompt(firstNumberMessege, num1);
        num2 = prompt(secondNumberMessege, num2);
    }

}
checkNumbers();

const operation = prompt("Enter what you want too do with this numbers: +, -, *  ");
let result;

function operanionWithNumbers(num1, num2, operation) {
    num1 = Number(num1);
    num2 = Number(num2);

    switch (operation) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        case "*":
            result = num1 * num2;
            break;
    }
    console.log(`${num1} ${operation} ${num2} = ${result}`);
    alert(result);
}

operanionWithNumbers(num1, num2, operation);


